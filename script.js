var btnBusca = document.querySelector('#btnBusca');
var results = document.querySelector('#results')
var arrayPego = []

btnBusca.addEventListener('click', () => {
    arrayPego = []
    var cidade = document.querySelector('#idCidade').value;
    var qtdDias = document.querySelector('#idQntDias').value
    if (cidade != '') {
        getTempo(cidade, qtdDias);
    }
})

function getTempo(cidade, qtdDias) {

    fetch(`https://weatherapi-com.p.rapidapi.com/forecast.json?q=${cidade}&days=${qtdDias}`, {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "weatherapi-com.p.rapidapi.com",
            "x-rapidapi-key": "f50c162d08mshb433874ef537aa6p1dc428jsn237e005b59a5"
        }
    }).then(response => {
        return response.json()
    })
        .then((data) => {
            console.log(data)
            var array = getDados(data.forecast.forecastday)
            imprimeTela(array)
        })
        .catch(err => {
            console.error(err);
        });
}

function getDados(array) {
    var cidadeEscolhida = document.querySelector('#idCidade').value;
    //pegar os dados e colocar no array
    for (var i = 0; i < array.length; i++) {
        let data = array[i].date;
        let cidade = cidadeEscolhida;
        let sunrise = array[i].astro.sunrise;
        let sunset = array[i].astro.sunset;
        let condition = array[i].day.condition.text
        let temperaturaMax =  array[i].day.maxtemp_c
        let temperaturaMin =  array[i].day.mintemp_c

        let objPego = criaObj(data, sunrise, sunset, cidade, condition,temperaturaMax,temperaturaMin)

        let aux = objPego

        arrayPego[i] = aux;

    }
    console.log(arrayPego)
    return arrayPego;
}

function getImg(txt) {
    let imgUrl
    if (txt === 'Partly cloudy') {
        console.log(txt === 'Partly cloudy');
        return "imgs/partly.png"
    } else if (txt === 'Patchy rain possible' || txt == 'Moderate rain') {
        console.log('entrei 2')
        console.log(txt === 'Patchy rain possible');
        return "imgs/rainy-day.png"
    }
    // if(txt==='')
    //se for sol um solzin e etc
}

function criaObj(data, sunrise, sunset, cidade, condition,temperaturaMax,temperaturaMin) {
    let objPego = {}
    objPego.data = data;
    objPego.sunrise = sunrise;
    objPego.sunset = sunset;
    objPego.cidade = cidade;
    objPego.condition = condition;
    objPego.temperaturaMax = temperaturaMax;
    objPego.temperaturaMin=temperaturaMin;
    return objPego;
}

function imprimeTela(array) {

    if (array.length <= 1) {
        results.innerHTML = `<div id="d1">
    <h2>Tempo para hoje em ${capitalizeFirstLetter(array[0].cidade)}</h2>
    <div id="titulos" class="row">
    <div id="titulo" class="col">Data</div>
    <div id="titulo"  class="col">Nascer do Sol</div>
    <div id="titulo"  class="col">Pôr do Sol</div>
    <div id="titulo"  class="col">Tempo</div>
    <div id="titulo"  class="col">Temperaturas</div>
    </div>
</div>`
    } else {

        results.innerHTML = `<div id="d1">
    <h2>Tempo para os próximos ${array.length} dias em ${capitalizeFirstLetter(array[0].cidade)}</h2>
    <div id="titulos" class="row">
        <div id="titulo" class="col">Data</div>
        <div id="titulo"  class="col">Nascer do Sol</div>
        <div id="titulo"  class="col">Pôr do Sol</div>
        <div id="titulo"  class="col">Tempo</div>
        <div id="titulo"  class="col">Temperaturas</div>
    </div>
</div>`
    }
    for (let i = 0; i < array.length; i++) {
        let obj = array[i];
        results.innerHTML += `
    <div id="d1">
        <div id="conteudo" class="row">
            <div id="resultadoData" class="col">
            
            ${quebraData(obj.data)}

            <img id="imgResults" src="imgs/calendar.png" alt=""> 

            </div>
            <div id="resultadoSunrise"  class="col">
            ${quebraHora(obj.sunrise)}
            <img id="imgResults" src="imgs/sunrise.png" alt=""> </div>
            <div id="resultadoSunset"  class="col">
            ${quebraHora(obj.sunset)}
            <img id="imgResults" src="imgs/sunset.png" alt=""> </div>
            
            <div id="resultadoCondicao"  class="col">
          ${obj.condition}
           
                <img id="imgResults" src="${getImg(obj.condition)}" alt="">         
        </div>
        <div id="resultadoTemperatura"  class="col">
        <p>Minima: ${obj.temperaturaMin}</p>
        <p>Máxima: ${obj.temperaturaMax}</p>
      </div>
    </div>
`
    }

}

function quebraData(data) {
    var array = data.split('-')
    return array[2] + "/" + array[1] + "/" + array[0]
}

function quebraHora(hora) {
    var array = hora.split(' ')
    if (array[1] === 'AM') {
        return array[0]
    } else {
        var split = array[0].split(':')
        let hora = parseInt(split[0])
        let minuto = parseInt(split[1])
        return hora + 12 + ":" + minuto

    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
